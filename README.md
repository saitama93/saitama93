# Hello world, je m'appel Igal. [Saitama][website] pour les intimes.


## Je suis un concepteur développeur d'application (PHP / Angular) qui s'intéresse aussi à la partie ops 😎

- Je suis actuellement en poste chez [SII][sii] en tant que développeur PHP/Angular
- Pour cette nouvelle année 2024 mes objectifs sont: 
    - Apprendre le TDD et l'architecture hexagonale 
    - 👨‍💻 Elargir mes connaissances en pratiquant le Devops


### Languages et technos utilisés:


![HTML](https://img.shields.io/badge/-HTML-05122A?style=flat&logo=HTML5)&nbsp;
![Angular](https://img.shields.io/badge/-Angular-05122A?style=flat&logo=angular)&nbsp;
![Jenkins](https://img.shields.io/badge/-Jenkins-05122A?style=flat&logo=jenkins)&nbsp;
![Sonar](https://img.shields.io/badge/-Sonar-05122A?style=flat&logo=sonar)&nbsp;
![CSS](https://img.shields.io/badge/-CSS-05122A?style=flat&logo=CSS3&logoColor=1572B6)&nbsp;
![JavaScript](https://img.shields.io/badge/-JavaScript-05122A?style=flat&logo=javascript)&nbsp;
![Bootstrap](https://img.shields.io/badge/-Bootstrap-05122A?style=flat&logo=bootstrap&logoColor=563D7C)&nbsp;
![TailwindCss](https://img.shields.io/badge/-TailwindCss-05122A?style=flat&logo=tailwindcss)&nbsp;
![Webpack](https://img.shields.io/badge/-Webpack-05122A?style=flat&logo=webpack)&nbsp;
![NPM](https://img.shields.io/badge/-NPM-05122A?style=flat&logo=NPM)\
![Wordpress](https://img.shields.io/badge/-Wordpress-05122A?style=flat&logo=wordpress)&nbsp;
![Symfony](https://img.shields.io/badge/-Symfony-05122A?style=flat&logo=symfony)&nbsp;
![php](https://img.shields.io/badge/-Php-05122A?style=flat&logo=php)\
![MongoDB](https://img.shields.io/badge/-MongoDB-05122A?style=flat&logo=mongodb)&nbsp;
![MySQL](https://img.shields.io/badge/-MySQL-05122A?style=flat&logo=mysql)\
![Docker](https://img.shields.io/badge/-Docker-05122A?style=flat&logo=docker)&nbsp;
![Git](https://img.shields.io/badge/-Git-05122A?style=flat&logo=git)&nbsp;
![Gitlab](https://img.shields.io/badge/-Gitlab-05122A?style=flat&logo=gitlab)&nbsp;
![Linux](https://img.shields.io/badge/-Linux-05122A?style=flat&logo=linux)&nbsp;


### Outils:

![Visual Studio Code](https://img.shields.io/badge/-Visual%20Studio%20Code-05122A?style=flat&logo=visual-studio-code&logoColor=007ACC)&nbsp;
![Figma](https://img.shields.io/badge/-Figma-05122A?style=flat&logo=figma)&nbsp;
![PhpStorm](https://img.shields.io/badge/-Phpstorm-05122A?style=flat&logo=phpstorm)&nbsp;
![Adobe XD](https://img.shields.io/badge/-Adobe%20XD-05122A?style=flat&logo=adobe-xd)
![Ubuntu](https://img.shields.io/badge/-Ubuntu-05122A?style=flat&logo=ubuntu)
<br />



Vous pouvez me contacter ici:

[![Website](https://img.shields.io/badge/Mon%20portfolio%20-igal--ilmi.fr-&?style=for-the-badge&logo=google&color=black)](https://igal-ilmi.fr)
[![Twitter](https://img.shields.io/twitter/follow/igalilmi32?color=1DA1F2&logo=twitter&style=for-the-badge)](https://twitter.com/igalilmi32)
[![Github](https://img.shields.io/badge/-@saitama93-05122A?style=flat&logo=github)](https://github.com/saitama93)
[![Linkedin](https://img.shields.io/badge/-igal--ilmi--amir-05122A?style=flat&logo=linkedin)](https://www.linkedin.com/in/igal-ilmi-amir/)


[website]: https://igal-ilmi.fr
[twitter]: https://twitter.com/igalilmi32
[linkedin]: https://www.linkedin.com/in/igal-ilmi-amir/
[sii]: https://sii-group.com/fr-FR
